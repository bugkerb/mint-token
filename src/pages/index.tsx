import type { NextPage } from 'next'
import { useEffect, useState } from 'react';
import Head from 'next/head'
import Link from 'next/link';
import { ethers } from 'ethers';
import contract from '../contracts/Gov.json'

const contractAddress = '0x08bC57d60256Ef429bf494cEc5eE3A02bC2403E2'
const abi = contract.abi
const gwei = 10


const Home: NextPage = () => {

  const [currentAccount, setCurrentAccount] = useState(null)
  const [mint, setMint] = useState('')
  const [mintResult, setMintResult] = useState('')
  const [tokenName, setTokenName] = useState('')
  const [tokenBalance, setTokenBalance] = useState(0.00)

  const checkWalletIsConnected = async () => {
    const { ethereum } = window
    if (!ethereum) {
      console.log("Make sure you have Metamask installed!")
      return
    } else {
      console.log("Wallet exists! We're ready to go!")
    }

    const accounts = await ethereum.request({ method: 'eth_accounts' })

    if (accounts.length !== 0) {
      const account = accounts[0]
      console.log("Found in authorized account: ", account)
      setCurrentAccount(account)
    } else {
      setCurrentAccount(null)
      console.log("No authorized account found")
    }
  }

  const connectWalletHandler = async () => {
    const { ethereum } = window
    if (!ethereum) {
      alert("Please install Metamask!")
    }

    try {
      const accounts = await ethereum.request({ method: 'eth_requestAccounts' })
      console.log("Found on account! Address: ", accounts[0])
      setCurrentAccount(accounts[0])
    } catch (err) {
      console.log(err)
    }
  }

  const mintTokenHandler = async () => {
    await checkWalletIsConnected()

    try {
      const { ethereum } = window

      if (ethereum && currentAccount) {
        const provider = new ethers.providers.Web3Provider(ethereum)
        const signer = provider.getSigner()
        const tokenContract = new ethers.Contract(contractAddress, abi, signer)
  
        const amount = mint
        const mintAmount = ethers.utils.parseEther(amount.toString())
        let gasLimit = await tokenContract.estimateGas.mint(mintAmount);
        console.log("gasLimit: ", gasLimit.toString())
        let txn = await tokenContract.mint(mintAmount, {
            gasPrice: ethers.utils.parseUnits(gwei.toString(), 'gwei'),
            gasLimit: gasLimit.toString()
        });
  
        const message: string = 'Minting... please wait'
        console.log(message)
        setMintResult(message)
        await txn.wait()
  
        const messageResult: any = <>Minted, see transaction:&nbsp;<a href={`https://testnet.bscscan.com/tx/${txn.hash}`} rel="noreferrer" target="_blank">Click Here!</a></>
        setMintResult(messageResult)
        console.log(`Minted, see transaction: https://testnet.bscscan.com/tx/${txn.hash}`)
  
        getTokenInfo()
      } else {
        console.log("Etherum object does not exist")
      }
    } catch (err: any) {
      if (err.message.indexOf(`unknown account #0`) == -1) {
        console.log(err)
      } 
    }
  }

  const connectWalletButton = () => {
    return (
      <button onClick={connectWalletHandler} className='btn btn-primary'>
        Connect Wallet
      </button>
    )
  }

  const changedMint = (e: any) => {
    setMint(e.target.value)
  }

  const mintTokenButton = () => {
    return (
      <div className='container'>
        <div className="input-group input-group-lg mb-3">
          <input type="text" className="form-control" id="mint" value={mint} onChange={changedMint} />
          <button onClick={mintTokenHandler} className="btn btn-warning" type="button" aria-required disabled={!mint}>Mint</button>
        </div>
      </div>
    )
  }

  const getTokenInfo = async () => {
    const { ethereum } = window
    console.log(`getTokenInfo: ${currentAccount}`)
    if (ethereum) {
      const provider = new ethers.providers.Web3Provider(ethereum)
      const signer = provider.getSigner()
   
      const tokenContract = new ethers.Contract(contractAddress, abi, signer)

      if (tokenName == '') {
        const symbol = await tokenContract.symbol()
        setTokenName(symbol)
      }
      
      const balance = await tokenContract.balanceOf(await signer.getAddress())
      setTokenBalance(+ethers.utils.formatUnits(balance.toString()))
    }
  }

  useEffect(() => {
    checkWalletIsConnected()
  }, [])

  useEffect(() => {
    if (currentAccount) {
      getTokenInfo()
    }
  }, [currentAccount])

  return (
    <div>
      <Head>
        <title>Mint Token</title>
        <meta name="description" content="Mint Token Example" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
    
      <main>
        {currentAccount ?
        <div className='container text-center'>
          <div className='row mt-5 mb-2 justify-content-center'>
            <div className='col-12'>
              <h4>
                <span className='alert alert-success'>Wallet:&nbsp;
                <Link href={`https://testnet.bscscan.com/address/${currentAccount}`}>
                  <a target='_blank' rel='noreferrer'>{currentAccount}</a>
                </Link></span>
              </h4>
            </div>
          </div>
          <div className='row mt-5 mb-2 justify-content-center'>
            <div className='col-12'>
              <h1 className='alert alert-secondary'>
                {`${tokenName}: ${tokenBalance}`}
              </h1>
            </div>
          </div>
          <div className='row mt-5 mb-2'>
            <h1>Mint Token</h1>
          </div>
          <div className='row justify-content-center'>
            <div className='col-sm-12 col-md-5'>
              {mintTokenButton()}
            </div>
          </div>
          <div className='row justify-content-center'>
            <div className='col-12 fw-bold'>
              {mintResult}
            </div>
          </div>
        </div>
        :
        <div className='container text-center'>
          <div className='row mt-5 mb-2 justify-content-center'>
            <div className='col-3'>
              {connectWalletButton()}
            </div>
          </div>
        </div>}
      </main>

      
    </div>
  )
}

export default Home
